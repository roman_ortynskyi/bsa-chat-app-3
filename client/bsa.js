import Chat from './src/components/chat/Chat';
import rootReducer from './src/store/rootReducer';

export default {
    Chat,
    rootReducer,
};