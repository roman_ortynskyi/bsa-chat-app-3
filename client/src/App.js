import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Routes from './navigation/Routes';
import store from './store/store';

import './App.css';

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Routes />
      </Router>
      <ToastContainer />
    </Provider>
    
  );
}

export default App;
