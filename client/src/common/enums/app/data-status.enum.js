const DataStatus = {
    IDLE: 'IDLE',
    PENDING: 'pending',
    FULFILLED: 'fulfilled',
    REJECTED: 'rejected'
};

export { DataStatus };
