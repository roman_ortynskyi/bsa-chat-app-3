const IconName = {
  USER_CIRCLE: 'user circle',
  LOG_OUT: 'log out',
  THUMBS_UP: 'thumbs up',
  THUMBS_DOWN: 'thumbs down',
  COMMENT: 'comment',
  SHARE_ALTERNATE: 'share alternate',
  FROWN: 'frown',
  IMAGE: 'image',
  COPY: 'copy',
  TRASH_ALTERNATE_OUTLINE: 'trash alternate outline',
  PENCIL: 'pencil',
  CHECK: 'check',
  CLOSE: 'close'
};

export { IconName };
