const IconSize = {
  SMALL: 'small',
  LARGE: 'large'
};

export { IconSize };
