import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Redirect, useHistory } from 'react-router-dom';
import { chatActionCreator } from '../../store/chat/actions';

import './EditMessage.css';

function EditMessage() {
    const [text, setText] = useState('');
    const { editingMessageId, messages } = useSelector(state => state.chat);
    const history = useHistory();
    const dispatch = useDispatch();
    
    useEffect(() => {
        if(editingMessageId) {
            const message = messages.find(m => m.id === editingMessageId)
            setText(message.text);
        }
        
    }, []);

    const saveChanges = () => {
        const message = messages.find(m => m.id === editingMessageId)
        const updatedMessage = {
            ...message,
            text
        };

        dispatch(chatActionCreator.updateMessage(updatedMessage));
        history.goBack();
    };

    const hideModal = () => {
        dispatch(chatActionCreator.setEditingMessageId(null));
        history.goBack();
    }

    return editingMessageId ? (
        <div id="demo-modal" className="edit-message-modal">
            <div className="modal__content">
                <h1>Edit Message</h1>

            <textarea 
                className="edit-message-input"
                value={text}
                onChange={(e) => setText(e.target.value)}
            ></textarea>

                <div className="modal__footer">
                    <button 
                        className="edit-message-button"
                        onClick={saveChanges}
                    >OK</button>
                    <button 
                        className="edit-message-close"
                        onClick={hideModal}
                    >Cancel</button>
                </div>
            </div>
        </div>
    )
    : <Redirect to="/chat" />;
};

export default EditMessage;