import React, { 
    useState, 
    useEffect, 
    createContext, 
    useRef
} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import ChatHeader from '../header/Header';
import MessageList from '../message-list/MessageList';
import MessageInput from '../message-input/MessageInput';
import Spinner from '../Spinner/Spinner';

import './Chat.css';
import { chatActionCreator } from '../../store/chat/actions';
import { DataStatus } from '../../common/enums/enums';
import { useHistory } from 'react-router-dom';


export const ChatContext = createContext();

function Chat({ url }) {
    const { messages, status, editModal } = useSelector(state => state.chat);
    const dispatch = useDispatch();
    const history = useHistory();

    const messageListRef = useRef();

    const contextValue = {
        messageListRef
    };

    useEffect(() => {
        dispatch(chatActionCreator.fetchMessages());
    }, [dispatch]);

    // const usersCount = new Set(messages.map(message => message.user)).size;
    // const lastMessageCreatedAt = messages[messages.length - 1]?.createdAt;

    const deleteMessage = (id) => {
        // const filteredMessages = messages.filter(message => message.id !== id);
        dispatch(chatActionCreator.deleteMessage(id));
    };

    const editMessage = (id) => {
        dispatch(chatActionCreator.setEditingMessageId(id));
        history.push('/editmessage');
    };

    const likeMessage = (id) => {
        // const messageIndex = messages.findIndex(el => el.id === id);

        // const message = messages[messageIndex];

        // const updatedMessage = {
        //     ...message,
        //     isLiked: !message.isLiked
        // }

        // const updatedMessages = [
        //     ...messages.slice(0, messageIndex),
        //     updatedMessage,
        //     ...messages.slice(messageIndex + 1)
        // ];

        // dispatch(chatActions.setMessages(updatedMessages));
    }

    const usersCount = new Set(messages.map(message => message.userId)).size;
    const lastMessageCreatedAt = messages[messages.length - 1]?.createdAt;

    return (
        <ChatContext.Provider value={contextValue}>
            <div className="chat">
                {status === DataStatus.PENDING ? <Spinner />
                : (
                    <>
                        <ChatHeader 
                            messagesCount={messages.length}
                            usersCount={usersCount}
                            lastMessageCreatedAt={lastMessageCreatedAt}
                        />
                        <MessageList
                            messages={messages} 
                            onDeleteMessage={deleteMessage} 
                            onEditMessage={editMessage} 
                            onLikeMessage={likeMessage}
                        />
                        <MessageInput />
                        {/* {editModal && <EditMessageModal />}  */}
                    </>
                )}
                
            </div>
        </ChatContext.Provider>
    );
}

export default Chat;