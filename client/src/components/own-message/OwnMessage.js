import React from 'react';
import moment from 'moment';

import './OwnMessage.css';

function OwnMessage(props) {
    const {
        id,
        text,
        createdAt,
        onEdit,
        onDelete
    } = props;

    const time = moment(createdAt).format('hh:mm');
    
    return (
        <div className="own-message you-message">
            <div className="message-content">
                <div className="message-text">{text}</div>
                <div className="message-time">{time}</div>
                <button className="message-edit" onClick={() => onEdit(id)}>Edit</button>
                <button className="message-delete" onClick={() => onDelete(id)}>Delete</button>  
            </div>
        </div>
    );
}

export default OwnMessage;