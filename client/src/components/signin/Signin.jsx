import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { DataStatus } from '../../common/enums/enums';
import { authActionCreator } from '../../store/auth/actions';
import Spinner from '../Spinner/Spinner';
import { toast } from 'react-toastify';
import './Signin.css';
import { useHistory } from 'react-router-dom';

function Signin() {
    // const alert = useAlert();
    const { status, error } = useSelector(state => state.auth);
    const dispatch = useDispatch();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const history = useHistory();

    const handleSubmit = (e) => {
        e.preventDefault();

        dispatch(authActionCreator.signIn({
            name: username,
            password
        }))
    };

    useEffect(() => {
        console.log(error);
        switch(status) {
            case DataStatus.REJECTED:
                toast(error);
                break;
            case DataStatus.FULFILLED:
                history.replace('/chat');
        }
    }, [status]);

    return (
        status === DataStatus.PENDING ? <Spinner/> : (
            <form onSubmit={handleSubmit}>
            <div class="signin-container">
                <label for="uname"><b>Username</b></label>
                <input 
                    type="text" 
                    placeholder="Enter Username"
                    value={username} 
                    onChange={(e) => setUsername(e.target.value)}
                />

                <label for="psw"><b>Password</b></label>
                <input 
                    type="password" 
                    placeholder="Enter Password" 
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                    
                <button 
                    type="submit"
                    className="signin-btn"
                >Sign in</button>
            </div>
            
        </form>
        )
    );
};

export default Signin;
