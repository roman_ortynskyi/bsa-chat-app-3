import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Signin from '../components/signin/Signin';
import Chat from '../components/chat/Chat';
import EditMessage from '../components/EditMessage/EditMessage';

function Routes() {
    return (
        <Switch>
            <Route exact path={`/`}>
                <Signin />
            </Route>
            <Route path={'/chat'}>
                <Chat />
            </Route>
            <Route path={'/editmessage'}>
                <EditMessage />
            </Route>
        </Switch>
    );
};

export default Routes;
