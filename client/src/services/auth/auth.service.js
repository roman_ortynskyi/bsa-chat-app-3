import { HttpMethod, ContentType } from "../../common/enums/enums";

export class AuthService {
    constructor({ http }) {
        this._http = http;
    }

    signIn(payload) {
        return this._http.load('http://localhost:3050/api/auth/signin', {
          method: HttpMethod.POST,
          contentType: ContentType.JSON,
          payload: JSON.stringify(payload)
        });
    }
}