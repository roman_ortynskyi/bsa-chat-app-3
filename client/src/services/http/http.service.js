import { getStringifiedQuery } from '../../helpers/helpers';
import { HttpHeader, HttpMethod } from '../../common/enums/enums';

export class HttpService {
  load(url, options = {}) {
    const {
      method = HttpMethod.GET,
      payload = null,
      contentType,
      query
    } = options;
    const headers = this._getHeaders({
      contentType
    });

    return fetch(this._getUrl(url, query), {
      method,
      headers,
      body: payload
    })
      .then(this._checkStatus)
      .then(this._parseJSON)
      .catch(this._throwError);
  }

  _getHeaders({ hasAuth, contentType }) {
    const headers = new Headers();

    if (contentType) {
      headers.append(HttpHeader.CONTENT_TYPE, contentType);
    }

    return headers;
  }

  async _checkStatus(response) {
    if (!response.ok) {
      const parsedException = await response.json();

      throw new Error(parsedException?.message ?? response.statusText);
    }

    return response;
  }

  _getUrl(url, query) {
    return `${url}${query ? `?${getStringifiedQuery(query)}` : ''}`;
  }

  _parseJSON(response) {
    if(response.status !== 204) return response.json();
  }

  _throwError(err) {
    throw err;
  }
}
