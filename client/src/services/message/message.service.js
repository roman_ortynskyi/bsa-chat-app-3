import { HttpMethod, ContentType } from "../../common/enums/enums";

export class MessageService {
    constructor({ http }) {
        this._http = http;
    }

    save(payload) {
        return this._http.load('http://localhost:3050/api/messages', {
          method: HttpMethod.POST,
          contentType: ContentType.JSON,
          payload: JSON.stringify(payload)
        });
    }

    findAll() {
        return this._http.load('http://localhost:3050/api/messages', {
          method: HttpMethod.GET
        });
    }
    
    update(id, payload) {
        return this._http.load(`http://localhost:3050/api/messages/${id}`, {
            method: HttpMethod.PUT,
            contentType: ContentType.JSON,
            payload: JSON.stringify(payload)
        });
    }
    
    delete(id) {
        return this._http.load(`http://localhost:3050/api/messages/${id}`, {
          method: HttpMethod.DELETE
        });
      }
}