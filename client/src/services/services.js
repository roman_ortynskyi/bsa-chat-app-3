import { AuthService } from './auth/auth.service';
import { UserService } from './user/user.service';
import { HttpService } from './http/http.service';
import { MessageService } from './message/message.service';


const httpService = new HttpService();

const authService = new AuthService({
  http: httpService
});

const userService = new UserService({
    http: httpService
});

const messageService = new MessageService({
    http: httpService
});

export { 
    httpService,
    authService,
    userService,
    messageService
};