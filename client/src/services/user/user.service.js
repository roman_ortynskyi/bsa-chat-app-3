import { HttpMethod, ContentType } from "../../common/enums/enums";

export class UserService {
    constructor({ http }) {
        this._http = http;
    }

    save(payload) {
        return this._http.load('http://localhost:3050/api/users', {
          method: HttpMethod.POST,
          contentType: ContentType.JSON,
          payload: JSON.stringify(payload)
        });
    }
    
    update(id, payload) {
        return this._http.load(`http://localhost:3050/api/users/${id}`, {
            method: HttpMethod.PUT,
            payload: JSON.stringify(payload)
        });
    }
    
    delete(id) {
        return this._http.load(`http://localhost:3050/api/users/${id}`, {
          method: HttpMethod.DELETE
        });
    }
}