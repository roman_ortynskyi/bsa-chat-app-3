import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import {
  authService
} from '../../services/services';

const ActionType = {
  SET_USER: 'profile/set-user',
  SIGN_IN: 'profile/sign-in'
};

const setUser = createAction(ActionType.SET_USER, user => ({
  payload: {
    user
  }
}));

const signIn = createAsyncThunk(ActionType.SIGN_IN, async (payload) => {
  const user = await authService.signIn(payload);

  return user;
});

const logout = () => dispatch => {
  dispatch(setUser(null));
};

const loadCurrentUser = () => async dispatch => {
  const user = await authService.getCurrentUser();

  dispatch(setUser(user));
};

const updateCurrentUser = updatedUser => async dispatch => {
  const user = await authService.updateCurrentUser(updatedUser);

  dispatch(setUser(user));
};

export const authActionCreator = {
  setUser,
  signIn,
  logout,
  loadCurrentUser,
  updateCurrentUser
};