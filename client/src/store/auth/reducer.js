import { createReducer } from '@reduxjs/toolkit';
import { DataStatus } from '../../common/enums/enums';
import {authActionCreator} from './actions';

const initialState = {
  user: null,
  status: DataStatus.IDLE,
  error: null
};

const {
    setUser,
    signIn
} = authActionCreator;

const reducer = createReducer(initialState, builder => {
  builder.addCase(setUser, (state, action) => {
    const { user } = action.payload;

    state.user = user;
  });

  builder.addCase(signIn.pending, (state, action) => {
      state.status = DataStatus.PENDING;
  });

  builder.addCase(signIn.fulfilled, (state, action) => {
    state.status = DataStatus.FULFILLED;
    state.user = action.payload;
  });

  builder.addCase(signIn.rejected, (state, action) => {
    state.status = DataStatus.REJECTED;
    state.error = 'Wrong name or password';
  }); 
});

export { reducer };
