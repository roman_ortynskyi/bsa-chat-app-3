import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import {
  messageService
} from '../../services/services';

const ActionType = {
  SET_MESSAGES: 'messages/set-messages',
  FETCH_MESSAGES: 'messages/fetch-messages',
  SEND_MESSAGE: 'messages/send-message',
  UPDATE_MESSAGE: 'messages/update-message',
  DELETE_MESSAGE: 'messages/delete-message',
  SET_EDITING_MESSAGE_ID: 'messages/set-editing-message-id'
};

const setMessages = createAction(ActionType.SET_MESSAGES, messages => ({
  payload: {
    messages
  }
}));

const setEditingMessageId = createAction(ActionType.SET_EDITING_MESSAGE_ID, editingMessageId => ({
  payload: {
    editingMessageId
  }
}));

const fetchMessages = createAsyncThunk(ActionType.FETCH_MESSAGES, async () => {
  const messages = await messageService.findAll();

  return messages;
});

const sendMessage = createAsyncThunk(ActionType.SEND_MESSAGE, async (message) => {
  const result = await messageService.save(message);

  return result;
});

const updateMessage = createAsyncThunk(ActionType.UPDATE_MESSAGE, async (message) => {
  const result = await messageService.update(message.id, message);

  return result;
});

const deleteMessage = createAsyncThunk(ActionType.DELETE_MESSAGE, async (id) => {
  await messageService.delete(id);
});

export const chatActionCreator = {
  setMessages,
  setEditingMessageId,
  fetchMessages,
  sendMessage,
  updateMessage,
  deleteMessage
};
