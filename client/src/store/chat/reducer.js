import { createReducer } from '@reduxjs/toolkit';
import { DataStatus } from '../../common/enums/enums';
import { chatActionCreator } from './actions';

const initialState = {
  messages: [],
  editingMessageId: null,
  status: DataStatus.IDLE,
  error: null
};

const { 
  deleteMessage, 
  fetchMessages, 
  sendMessage, 
  setMessages, 
  setEditingMessageId 
} = chatActionCreator;

const reducer = createReducer(initialState, builder => {
  builder.addCase(setMessages, (state, action) => {
    const { messages } = action.payload;

    state.messages = messages;
  });

  builder.addCase(setEditingMessageId, (state, action) => {
    const { editingMessageId } = action.payload;

    state.editingMessageId = editingMessageId;
  });

  // fetchMessages 
  builder.addCase(fetchMessages.pending, (state, action) => {
    state.status = DataStatus.PENDING;
  });

  builder.addCase(fetchMessages.fulfilled, (state, action) => {
    state.status = DataStatus.FULFILLED;
    state.messages = action.payload;
  });

  builder.addCase(fetchMessages.rejected, (state, action) => {
    state.status = DataStatus.REJECTED;
  });

  // sendMessage
  builder.addCase(sendMessage.fulfilled, (state, action) => {
    state.status = DataStatus.FULFILLED;
    state.messages = state.messages.concat([action.payload]);
  });

  builder.addCase(sendMessage.rejected, (state, action) => {
    state.status = DataStatus.REJECTED;
  });

  // deleteMessage
  builder.addCase(deleteMessage.fulfilled, (state, action) => {
    state.status = DataStatus.FULFILLED;
    state.messages = state.messages.filter(message => message.id !== action.meta.arg);
  });

  builder.addCase(deleteMessage.rejected, (state, action) => {
    state.status = DataStatus.REJECTED;
  });
});

export { reducer };
