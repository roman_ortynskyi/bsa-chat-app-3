import { configureStore } from '@reduxjs/toolkit';
import { reducer as auth } from './auth/reducer';
import { reducer as chat } from './chat/reducer';

const store = configureStore({
  reducer: {
    auth,
    chat
  }
});

export default store;
