const responseMiddleware = (req, res, next) => {
    if(req.result) {
        const {
            status,
            body,
        } = req.result;
        return res.status(status).json(body);
    }
}

module.exports = responseMiddleware;
