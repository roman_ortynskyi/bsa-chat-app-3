const fs = require('fs');
const { v4: uuid } = require('uuid');

let messages = [
    {
        "id": "jlkjl",
        "userId": 2,
        "text": "hello",
        "createdAt": "2021-07-16T12:55:16.949Z"
    },
    {
        "id": "kkhkgjhf",
        "userId": 1,
        "text": "d",
        "createdAt": "2021-07-16T16:55:16.949Z"
    },
    {
        "id": "79878",
        "userId": 2,
        "text": "dddddd",
        "createdAt": "2021-07-16T17:55:16.949Z"
    },
    {
        "id": "08798",
        "userId": 1,
        "text": "qqqqqqqqqqq",
        "createdAt": "2021-07-16T19:55:16.949Z"
    },
];

class MessageRepository {
    static save(message) {
        const id = uuid();
        const createdMessage = {
            ...message,
            id
        };

        const updatedMessages = [
            ...messages,
            createdMessage
        ];

        messages = updatedMessages;

        return createdMessage;
    }

    static findAll() {
        return messages;
    }

    static update(id, message) {
        const index = messages.findIndex(m => m.id === id);
        messages[index] = {
            id,
            ...message
        };

        return messages[index];
    }

    static delete(id) {
        const updatedMessages = messages.filter(m => m.id !== id);

        messages = updatedMessages;
    }
}

module.exports = MessageRepository;
