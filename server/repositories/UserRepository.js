const fs = require('fs');
const { v4: uuid } = require('uuid');

let users = [
    {
        "id": 0,
        "name": "admin",
        "password": "admin",
        "email": "admin@gmail.com"
    },
    {
        "id": 1,
        "name": "roman",
        "password": "123",
        "email": "roman@gmail.com"
    },
    {
        "id": 2,
        "name": "vasya",
        "password": "123",
        "email": "vasya@gmail.com"
    },
    {
        "id": 3,
        "name": "petro",
        "password": "123",
        "email": "petro@gmail.com"
    }
];

class UserRepository {
    static save(user) {
        const id = uuid();
        const createdUser = {
            ...user,
            id
        };

        const updatedUsers = [
            ...users,
            createdUser
        ];

        users = updatedUsers;

        return createdUser;
    }

    static findByNameAndPassword(name, password) {
        const user = users.find(user => user.name === name && user.password === password);

        return user;
    }

    static update(id, user) {
        const index = users.findIndex(u => u.id === id);
        users[index] = {
            id,
            ...user
        };

        return users[index];
    }

    static delete(id) {
        const updatedUsers = users.filter(u => u.id !== id);

        users = updatedUsers;
    }
}

module.exports = UserRepository;
