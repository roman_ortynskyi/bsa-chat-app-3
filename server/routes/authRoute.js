const { Router } = require('express');
const AuthService = require('../services/AuthService');

const router = Router();

router.post('/signin', (req, res, next) => {
    const {
        name,
        password
    } = req.body;

    const user = AuthService.signIn(name, password);
    req.result = user ? {
        body: user,
        status: 200
    } : {
        body: 'Incorrect email or password',
        status: 401
    }

    next();
})

module.exports = router;