const authRoute = require('./authRoute');
const userRoute = require('./userRoute');
const messageRoute = require('./messageRoute');

module.exports = (app) => {
    app.use('/api/auth', authRoute);
    app.use('/api/users', userRoute);
    app.use('/api/messages', messageRoute);
};