const { Router } = require('express');
const MessageService = require('../services/MessageService');

const router = Router();

router.post('/', (req, res, next) => {
    const user = MessageService.save(req.body);
    req.result = {
        body: user,
        status: 200
    };

    next();
});

router.get('/', (req, res, next) => {
    const messages = MessageService.findAll();
    req.result = {
        body: messages,
        status: 200
    };

    next();
});

router.put('/:id', (req, res, next) => {
    const user = MessageService.update(req.params.id, req.body);
    req.result = {
        body: user,
        status: 200
    };

    next();
});

router.delete('/:id', (req, res, next) => {
    MessageService.delete(req.params.id);
    req.result = {
        status: 204
    };

    next();
});

module.exports = router;