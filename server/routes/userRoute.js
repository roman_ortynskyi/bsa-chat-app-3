const { Router } = require('express');
const UserService = require('../services/UserService');

const router = Router();

router.post('/', (req, res, next) => {
    const user = UserService.save(req.body);
    req.result = {
        body: user,
        status: 200
    };

    next();
});

router.put('/:id', (req, res, next) => {
    const user = UserService.update(req.params.id, req.body);
    req.result = {
        body: user,
        status: 200
    };

    next();
});

router.delete('/:id', (req, res, next) => {
    UserService.delete(req.params.id);
    req.result = {
        status: 204
    };

    next();
});

module.exports = router;