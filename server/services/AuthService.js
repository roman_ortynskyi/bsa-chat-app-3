const UserRepository = require('../repositories/UserRepository');

class AuthService {
    static signIn(username, password) {
        const user = UserRepository.findByNameAndPassword(username, password);
        if(user) {
            const { id, name } = user;
            const role = name === 'admin' ? 'admin' : 'user';
            return {
                id,
                role
            };
        }
        
        return null;
    }
}

module.exports = AuthService;
