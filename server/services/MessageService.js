const MessageRepository = require('../repositories/MessageRepository');

class MessageService {
    static save(message) {
        const createdMessage = MessageRepository.save(message);

        return createdMessage;
    }

    static findAll() {
        const messages = MessageRepository.findAll();

        return messages;
    }

    static update(id, message) {
        const updatedMessage = MessageRepository.update(id, message);

        return updatedMessage;
    }

    static delete(id) {
        MessageRepository.delete(id);
    }
}

module.exports = MessageService;
