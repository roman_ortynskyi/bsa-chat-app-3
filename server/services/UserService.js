const UserRepository = require('../repositories/UserRepository');

class UserService {
    static save(user) {
        const createdUser = UserRepository.save(user);

        return createdUser;
    }

    static update(id, user) {
        const updatedUser = UserRepository.update(id, user);

        return updatedUser;
    }

    static delete(id) {
        UserRepository.delete(id);
    }
}

module.exports = UserService;
